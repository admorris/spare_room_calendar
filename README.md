# Location occupancy calendar

Generate a simple HTML calendar based on events in an iCal calendar on a CalDAV
server. The content of the location field is used to select which events to use.
The output calendars show when the location is busy/occupied. The use-case is
e.g. guests staying in a spare room. The events are assumed to be non-overlapping
and span more than one day.

## Installation

This is a pip-installable python package.

## Configuration

A JSON config file should be written with the following structure:

```json
{
  "calendar": "Personal",
  "credentials": {
    "url": "https://nextcloud.example.com/remote.php/dav",
    "username": "bob",
    "password": "12345"
  },
  "location": "Spare room",
  "n_years": 2
}
```

- "calendar" should correspond to the calendar name on your CalDAV server
- "location" is used to filter iCal events by the location field
- "n_years" sets the number of pages to generate: one per year

An optional key "year" sets the starting year, which is otherwise assumed to be
the current year.

## Usage

Installing the python module gives one CLI command: `publish_calendar`.

Syntax: `publish_calendar [-h] [--config CONFIG] destination`

positional arguments:  
- `destination`:      Directory to write the web pages.

optional arguments:  
- `-h, --help`:       show this help message and exit
- `--config CONFIG`:  JSON file containing the configuration

The names of generated files are printed when the programme runs.

Example:

```
$ publish_calendar --config config.json .
2023.html
2024.html
calendar.css
```

