#    Copyright (C) 2023     Adam Morris
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from caldav.davclient import DAVClient
from icalendar import Event
from datetime import date, datetime, timedelta

def fetch_events(config):
    dc = DAVClient(**config["credentials"])
    cal = dc.principal().calendar(config["calendar"])
    evts = cal.search(
        event = True,
        location = config["location"],
    )

    dates = {
        "arrival": set([]),
        "departure": set([]),
        "busy": set([]),
    }

    for evt in evts:
        e = Event.from_ical(evt.data).subcomponents[0]
        start = e["dtstart"].dt
        end = e["dtend"].dt
        if(isinstance(start, datetime)):
            start = start.date()
        if(isinstance(end, date) and not isinstance(end, datetime)):
            end -= timedelta(days=1)
        if(isinstance(end, datetime)):
            end = end.date()
        delta = end - start
        dates["arrival"].add(start)
        for i in range(1, delta.days):
            dates["busy"].add(start+timedelta(days=i))
        dates["departure"].add(end)

    # For days that are both arrival and departure: set to "busy"
    dates["busy"] |= (dates["arrival"] & dates["departure"])

    # Remove dates marked "busy" from arrival/departure dates
    for status in ["arrival", "departure"]:
        dates[status] -= dates[status] & dates["busy"]

    return dates

