#    Copyright (C) 2023     Adam Morris
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import json
from pathlib import Path
from argparse import ArgumentParser
from datetime import datetime

from .caldav import fetch_events
from .publish import make_calendar_page, write_page, write_css

def load_config(config_file):
    with open(config_file, "r") as f:
        return json.load(f)

def main():
    parser = ArgumentParser(prog="publish_calendar", description="Write HTML pages containing calendars showing location-occupancy data")
    parser.add_argument("destination", type=Path, help="Directory to write the web pages.")
    parser.add_argument("--config", type=Path, default="config.json", help="JSON file containing the configuration.")
    args = parser.parse_args()

    config = load_config(args.config)
    dest_dir = args.destination
    dest_dir.mkdir(parents=True, exist_ok=True)

    dates = fetch_events(config)
    current_year = config.get("year", datetime.today().year)

    for year in range(current_year, current_year + config["n_years"]):
        page = make_calendar_page(year, dates, config["location"])
        write_page(page, dest_dir / f"{year}.html")
    write_css(dest_dir / "calendar.css")

