#    Copyright (C) 2023     Adam Morris
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from calendar import HTMLCalendar
from lxml import html
from datetime import date, datetime

def get_title_cell(table):
    top_row = table.getchildren()[0]
    title_cell = top_row.getchildren()[0]
    title_cell.classes.add("title")
    return title_cell

def process_calendar(table, dates, current_date):
    get_title_cell(table)
    for row in table.getchildren()[1:]:
        for cell in row.getchildren():
            to_print = cell.text
            try:
                current_date["day"] = int(cell.text)
                today = date(**current_date)
                if today < date.today():
                    cell.classes.add("past")
                for status in dates:
                    if today in dates[status]:
                        cell.classes.add(status)
                if today not in set().union(*dates.values()):
                    cell.classes.add("free")
            except ValueError:
                continue

def make_calendar_page(year, dates, location):
    hc = HTMLCalendar()
    html_data = hc.formatyearpage(year, width=2, encoding="utf-8")
    page = html.document_fromstring(html_data)
    current_date = {
        "year": year,
        "month": 0,
        "day": None,
    }
    # Loop through the rows containing the table for each month
    outer_table = page.body.getchildren()[0]
    title_cell = get_title_cell(outer_table)
    title_cell.text = f"{location} occupancy {year}"
    for outer_row in outer_table.getchildren()[1:]:
        for table_cell in outer_row.getchildren():
            # Ensure vertical alignment
            table_cell.classes.add("align-top")
            table = table_cell.getchildren()[0]
            current_date["month"] += 1
            process_calendar(table, dates, current_date)

    now = datetime.now().astimezone().strftime("%Y-%m-%d %H:%M %Z")
    page.head.append(html.fragment_fromstring('<meta name="viewport" content="width=device-width, initial-scale=1.0">'))
    page.body.append(html.fragment_fromstring(f"<footer>Last updated: {now}</footer>"))
    return page

def write_page(page, filename):
    print(filename)
    with open(filename, "wb") as f:
        f.write(html.tostring(page, pretty_print=True))

CSS_DATA = """@charset "UTF-8";
/* General style */
body {
    font-family: "PT Sans", sans-serif;
    color: #2d2d2d;
}
/* Calendar style */
:root {
    --busy-colour: salmon;
}
table {
    margin: 10px;
}
td {
    min-width: 40px;
    height: 40px;
    text-align: center;
    font-size: 20px;
}
.align-top {
    vertical-align: top;
}
.title {
    font-size: 2em;
}
.past {
    opacity: 0.7;
}
.busy {
    background-color: var(--busy-colour);
}
.arrival {
    background-color: var(--busy-colour);
    background: radial-gradient(circle 80px at 40px, var(--busy-colour) 50%, transparent 50%);
}
.departure {
    background-color: var(--busy-colour);
    background: radial-gradient(circle 80px at -30px, var(--busy-colour) 50%, transparent 50%);
}
"""

def write_css(filename):
    print(filename)
    with open(filename, "w") as f:
        f.write(CSS_DATA)

